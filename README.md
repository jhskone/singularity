![ |small ](imgs/cont.png)

# Docker and Singularity: Using Containers on Midway

This workshop will provide an overview of the landscape of container technologies and the concepts related to them. Containers offer a way to package software applications and workflows to make them reproducible, shareable, and portable. The most well known container platform, Docker, does not find direct use on high performance computing (HPC) systems like Midway due to security concerns and lack of support for communication libraries like MPI. Other emerging container platforms such as Singularity and Shifter were specifically developed with HPC in mind, offering a means to securely run containers on HPC resources. In this workshop we will specifically focus on the use of Singularity with Midway, providing examples of creating containerized applications and using the singularity runtime to run native singularity images and make compatible the use of Docker containers on HPC resources.


## Objectives

Attendees should be able to:

* Have familiarity with the concepts and language of containers

* Be able to run containers on Midway2

* Understand best practices for using containers on HPC systems

## Materials

* [Workshop Presentation](docs/containers.pdf)

* [Connecting to Midway2](docs/connecting_to_midway2.pdf)

## Useful References
[Docker Documentation](https://docs.docker.com/)

[Singularity Offiical Tutorial: Creating and running software containers with Singularity](https://github.com/ArangoGutierrez/Singularity-tutorial)

[Singularity Documentation](https://sylabs.io/docs/)